(setq ediff-split-window-function 'split-window-horizontally)
(setq ediff-window-setup-function 'ediff-setup-windows-plain)
(defun my/command-line-diff (switch)
  ;;(setq initial-buffer-choice nil)
  (let ((file1 (pop command-line-args-left))
    (file2 (pop command-line-args-left)))
    (ediff-files file1 file2)))
;; show the ediff command buffer in the same frame
(add-to-list 'command-switch-alist '("-diff" . my/command-line-diff))