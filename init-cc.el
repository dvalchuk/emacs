;;;
;;; C++ related configurations
;;;

(eval-and-compile
  (defun sdnas-mode-load-path ()
    (concat dotfiles-dir "sdnas-mode")))

(use-package sdnas-mode
  :load-path (lambda () (list (sdnas-mode-load-path)))
  :init
  (setq sdnas-mode-keymap-prefix (kbd "C-c s"))
  (add-hook 'c++-mode-hook #'sdnas-conditionally-enable)
  (add-hook 'c-mode-common-hook #'sdnas-conditionally-enable)
  (add-hook 'yaml-mode-hook #'sdnas-conditionally-enable)
  (add-hook 'makefile-mode-hook #'sdnas-conditionally-enable)
  )

(use-package waf-mode
  :ensure t
  :pin melpa
  :init
  (setq waf-mode-keymap-prefix (kbd "C-c w"))
  (add-hook 'python-mode-hook #'waf-conditionally-enable)
  (add-hook 'c++-mode-hook #'waf-conditionally-enable)
  (add-hook 'c-mode-common-hook #'waf-conditionally-enable)
  )

(defun dv-before-save-hook()
  "run before file save"
  (unless (and (boundp 'sdnas-mode) sdnas-mode)
    (delete-trailing-whitespace)
    )
  )

(defun dv-cpp-mode-hook ()
  (local-set-key (kbd "C-c b") 'compile)                                             ; KBD
  (local-set-key  (kbd "M-o") 'ff-find-other-file)                                   ; KBD
  ;;(local-set-key  (kbd "M-o") 'helm-projectile-find-other-file)                      ; KBD
  (setq-default ff-always-try-to-create nil)                                         ; do not try to create if missing
  (defvar dv-cpp-other-file-alist
    '(("\\.cpp\\'" (".h" ".hpp" ".ipp"))
      ("\\.ipp\\'" (".h" ".hpp" ".cpp"))
      ("\\.hpp\\'" (".cpp" ".h" ".ipp"))
      ("\\.cxx\\'" (".h" ".hxx" ".ixx"))
      ("\\.hxx\\'" (".cxx" ".h" ".ixx"))
      ("\\.c\\'" (".h"))
      ("\\.h\\'" (".c" ".cpp" ".hpp"))
      )
    )
  (setq-default ff-other-file-alist 'dv-cpp-other-file-alist)
  ;; todo move sdnas related include path's to the corresponding module
  (defvar dv-cpp-other-dirs-alist
    '("." ".." "src" "../src" "../include" "../inc" "../include/detail" "../detail" "./include" "../cfg")
    )
  (setq-default ff-search-directories 'dv-cpp-other-dirs-alist)
  (setq indent-tabs-mode nil)
  (setq c-basic-offset 4)
  (setq c-default-style "linux" c-basic-offset 4)
  (c-set-offset 'inline-open 0)
  (c-set-offset 'substatement-open 0)
  (c-set-offset 'innamespace 0)
  (setq-local eldoc-documentation-function #'ignore)
  (add-hook 'before-save-hook 'dv-before-save-hook nil 'make-it-local)
  )

(add-hook 'c++-mode-hook #'dv-cpp-mode-hook)
(add-hook 'c-mode-common-hook #'dv-cpp-mode-hook)

(defun dv-setup-compile-buffer ()
  "Enables ansi-colors and scrolling in the compilation buffer."
  (interactive)
  (add-hook 'compilation-filter-hook
            (lambda ()
              (when (eq major-mode 'compilation-mode)
                (ansi-color-apply-on-region compilation-filter-start (point-max)))))
  ;;(setq compilation-scroll-output t)
  (setq compilation-scroll-output 'first-error)
  (setq compilation-skip-threshold 2))

(dv-setup-compile-buffer)

(defun inside-enum-class-p (pos)
  "Checks if POS is within the braces of a C++ \"enum class\"."
  (ignore-errors
    (save-excursion
      (goto-char pos)
      (up-list -1)
      (backward-sexp 1)
      (looking-back "enum[ \t]+class[ \t]+[^}]+")
      )
    )
  )

(defun align-enum-class (langelem)
  (if (inside-enum-class-p (c-langelem-pos langelem))
      0
    (c-lineup-topmost-intro-cont langelem)
    )
  )

(defun align-enum-class-closing-brace (langelem)
  (if (inside-enum-class-p (c-langelem-pos langelem))
      '-
    '+)
  )

(defun fix-enum-class ()
  "Setup `c++-mode' to better handle \"class enum\"."
  (add-to-list 'c-offsets-alist '(topmost-intro-cont . align-enum-class))
  (add-to-list 'c-offsets-alist '(statement-cont . align-enum-class-closing-brace))
  )

;;;(add-hook 'c++-mode-hook #'fix-enum-class)

(defun dv-cpp-generate-include-guard-str()
  (setq filtered-path ())
  (setq ignoring (list "src" "include" "inc" "home" user-login-name))
  (let (value)
    (dolist (pitem (split-string (buffer-file-name) "/") value)
      (when (and
             (not (zerop (length pitem)))
             (not (member pitem ignoring)))
        (add-to-list 'filtered-path pitem))
      (setq value (cons pitem value))))
  (replace-regexp-in-string "[.-]" "_"
                            (upcase (mapconcat 'identity (reverse filtered-path) "_"))))

(defun dv-cpp-insert-include-guard()
  (interactive)
  (let ((guard-str (dv-cpp-generate-include-guard-str)))
    (save-excursion
      (beginning-of-buffer)
      (insert (concat "#ifndef " guard-str "\n"))
      (insert (concat "#define " guard-str "\n"))
      (end-of-buffer)
      (insert (concat "\n#endif // " guard-str "\n")))))

(add-to-list 'auto-mode-alist '("\\Makefile.inc\\'" . makefile-mode))
