(when (string-equal system-type "darwin")
  (setenv "PAGER" "cat")
  (setq py-python-command "python3"))

(defun dv-python-mode-hook ()
  (setq indent-tabs-mode nil)
  (setq python-indent-offset 4))

(add-hook 'python-mode-hook #'dv-python-mode-hook)
