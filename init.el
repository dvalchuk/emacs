;; package
;; Keep track of loading time
(defconst emacs-start-time (current-time))
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(let ((elapsed (float-time (time-subtract (current-time) emacs-start-time))))
  (message "Loaded packages in %.3fs" elapsed)
  )

(setq gc-cons-threshold (* 100 1024 1024)) ;; 100 mb
;; Allow font-lock-mode to do background parsing
(setq jit-lock-stealth-time 1
      ;; jit-lock-stealth-load 200
      jit-lock-chunk-size 1000
      jit-lock-defer-time 0.05)

(defun ensure-package-installed (&rest packages)
  "Assure every package is installed, ask for installation if it’s not. Return a list of installed packages or nil for every skipped package."
  (mapcar
   (lambda (package)
     ;; (package-installed-p 'evil)
     (if (package-installed-p package)
         nil
       (if (y-or-n-p (format "Package %s is missing. Install it? " package))
           (package-install package)
         package)
       )
     ) packages)
  )

;; make sure to have downloaded archive description.
;; Or use package-archive-contents as suggested by Nicolas Dudebout
(or (file-exists-p package-user-dir) (package-refresh-contents))
(ensure-package-installed 'use-package 'helm-projectile) ;  --> (nil nil) if iedit and magit are already installed
;; activate installed packages
(setq package-enable-at-startup t)

(require 'use-package)

;;; add current dir to load path
(setq dotfiles-dir (file-name-directory
                    (or (buffer-file-name) load-file-name)))
(add-to-list 'load-path dotfiles-dir)

(global-eldoc-mode -1)
(setq eldoc-idle-delay 1)

(load (concat dotfiles-dir "basic.el"))

(use-package doom-themes
  :ensure t
  :config
  (load-theme 'doom-dark+ t)
  (doom-themes-visual-bell-config))

;;unique buffer name
(use-package uniquify
  :init
  (setq uniquify-buffer-name-style 'forward)
  )

;; Sets up the with-editor package so things that invoke $EDITOR will use the current emacs if I'm already inside of emacs
(use-package with-editor
  :ensure t
  :pin melpa
  :init
  (progn
    (add-hook 'shell-mode-hook  #'with-editor-export-editor)
    (add-hook 'eshell-mode-hook #'with-editor-export-editor)
    )
  )

(use-package auto-complete
  :ensure t
  :pin melpa
  :init
  (progn
    (ac-config-default)
    (setq ac-ignore-case 'smart)
    )
  )

;; multi-web-mode
(when (and (boundp 'user-local-enable-web) user-local-enable-web)
  (message ">> enable WEB mode")
  (use-package multi-web-mode
    :ensure t
    :init
    (message ">> init WEB mode")
    (use-package php-mode
      :ensure t
      )
    (setq mweb-default-major-mode 'html-mode)
    (setq mweb-tags '((php-mode "<\\?php\\|<\\? \\|<\\?=" "\\?>")
                      (js-mode "<script +\\(type=\"text/javascript\"\\|language=\"javascript\"\\)[^>]*>" "</script>")
                      (css-mode "<style +type=\"text/css\"[^>]*>" "</style>")))
    (setq mweb-filename-extensions '("php" "htm" "html" "ctp" "phtml" "php4" "php5"))
    (multi-web-global-mode t)
    )
  )

; Expand-region.el
(use-package expand-region
  :ensure t
  :bind ("C-c =" . er/expand-region)
  )

;; Inno Setup mode
;; migrate to use-package if needed
(when (and (boundp 'user-local-enable-inno) user-local-enable-inno)
  (message ">> enable Inno Setup mode")
  (add-to-list 'load-path (concat dotfiles-dir "iss-mode"))
  (autoload 'iss-mode "iss-mode" "Innosetup Script Mode" t)
  (setq auto-mode-alist (append '(("\\.iss$"  . iss-mode)) auto-mode-alist))
  )

(when (eq system-type 'windows-nt)
  (setq delete-by-moving-to-trash t)
  )

;;; for open file via scp/sudo/su etc
(unless (eq system-type 'windows-nt)
  (setq tramp-default-method "scp")
)

(require 'desktop)
(require 'server)

;; save session
(desktop-save-mode 1)
(setq desktop-restore-frames t)
;;(setq desktop-restore-forces-onscreen nil) ;; fix http://stackoverflow.com/a/26546872/658346

;; whitespace mode
(use-package whitespace
  :bind ([f6] . whitespace-mode)
  :config
  (progn
    (setq whitespace-display-mappings '(
                                        (space-mark   ?\     [?\u00B7]     [?.])
                                        (space-mark   ?\xA0  [?\u00A4]     [?_])
                                        (newline-mark ?\n    [?¶ ?\n])
                                        (tab-mark     ?\t    [?\u00BB ?\t] [?\\ ?\t])
                                        ))

    ;; lines lines-tail newline trailing space-before-tab space-afte-tab empty
    ;; indentation-space indentation indentation-tab tabs spaces
    (setq whitespace-style '(face space-mark tab-mark newline-mark) )
    (custom-set-faces
     '(whitespace-space ((t (:bold t :foreground "gray75"))))
     '(whitespace-empty ((t (:foreground "firebrick" :background "SlateGray1"))))
     '(whitespace-hspace ((t (:foreground "lightgray" :background "LemonChiffon3"))))
     '(whitespace-indentation ((t (:foreground "firebrick" :background "beige"))))
     '(whitespace-line ((t (:foreground "black" :background "red"))))
     '(whitespace-newline ((t (:foreground "orange" :background "blue"))))
     '(whitespace-space-after-tab ((t (:foreground "black" :background "green"))))
     '(whitespace-space-before-tab ((t (:foreground "black" :background "DarkOrange"))))
     '(whitespace-tab ((t (:foreground "blue" :background "white"))))
     '(whitespace-trailing ((t (:foreground "red" :background "yellow"))))
     )
    )
  )

;; KBD
;; user-local-root-path should be defined in ~/.emacs file
(global-set-key [f2] (lambda()
                       (interactive)
                       (find-file (concat user-local-root-path "/init.el"))
                       )
                )
(global-set-key (kbd "C-c <deletechar>") 'kill-whole-line)
(if (>= emacs-major-version 26)
  (global-set-key [f7] 'display-line-numbers-mode)
  (global-set-key [f7] 'linum-mode)
  )
(global-set-key (kbd "<f9>") 'bookmark-set)
(global-set-key (kbd "C-<f9>") 'bookmark-bmenu-list)


;; Open files and go places like we see from error messages, i e: path:line:col
;; (to-do "make `find-file-line-number' work for emacsclient as well")
;; (to-do "make `find-file-line-number' check if the file exists")
(defadvice find-file (around find-file-line-number
                             (path &optional wildcards)
                             activate
                             )
  "Turn files like file.js:14:10 into file.js and going to line 14, col 10."
  (save-match-data
    (let* ((match (string-match "^\\(.*?\\):\\([0-9]+\\):?\\([0-9]*\\):*$" path))
           (line-no (and match
                         (match-string 2 path)
                         (string-to-number (match-string 2 path))
                         )
                    )
           (col-no (and match
                        (match-string 3 path)
                        (string-to-number (match-string 3 path))
                        )
                   )
           (path (if match (match-string 1 path) path))
           )
      ad-do-it
      (when line-no
        ;; goto-line is for interactive use
        (goto-char (point-min))
        (forward-line (1- line-no))
        (when (> col-no 0)
          (forward-char (1- col-no))
          )
        )
      )
    )
  )

;; HELM
(use-package helm
  :ensure t
  :init
    (helm-mode 1)
  :diminish "helm-mode"
  :config
  (progn
    (use-package helm-config)
    (set-face-attribute 'helm-selection nil
                    :background "purple"
                    :foreground "black")
    (setq helm-split-window-in-side-p           t ; open helm buffer inside current window, not occupy whole other window
          helm-move-to-line-cycle-in-source     t ; move to end or beginning of source when reaching top or bottom of source.
          helm-ff-search-library-in-sexp        t ; search for library in `require' and `declare-function' sexp.
          helm-scroll-amount                    8 ; scroll 8 lines other window using M-<next>/M-<prior>
          helm-source-bookmarks                 t
          helm-ff-file-name-history-use-recentf t)
    ;;(set-face-attribute 'helm-selection nil :background "purple" :foreground "black")
    (define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action) ; rebind tab to run persistent action
    (define-key helm-map (kbd "C-i") 'helm-execute-persistent-action) ; make TAB works in terminal
    (define-key helm-map (kbd "C-z") 'helm-select-action) ; list actions using C-z
    (use-package helm-projectile
      :bind (
             ([f3] . helm-projectile)
             ([f4] . helm-find-files)
             ("C-x C-f" . helm-find-files)
             )
      )
    (use-package helm-ag
      :ensure t
      )
    (helm-mode)
    )
  :bind (
         ("C-c h" . helm-command-prefix)
         ("C-x C-b" . helm-buffers-list)
         ("M-y" . helm-show-kill-ring)
         ("M-x" . helm-M-x)
         ([f10] . helm-recentf)
         ("C-<f3>" . helm-semantic-or-imenu)
         ("C-h SPC" . helm-all-mark-rings)
         ("C-<f4>" . helm-projectile-ag)
         )
  )

;;projectile
(use-package projectile
  :config
  (setq projectile-completion-system 'helm
        projectile-enable-caching    t
        projectile-globally-ignored-files
        (append '(".DS_Store"
                  ".pyc"
                  ".class"
                  ".gz"
                  ".jar"
                  ".tar.gz"
                  ".tgz"
                  ".zip"
                  "~")
                projectile-globally-ignored-files)
        projectile-globally-ignored-directories
        (append '(
                  ".git"
                  ".svn"
                  "lost+found"
                  )
                projectile-globally-ignored-directories)
        projectile-globally-unignored-directories (append '("sade/src/dart/Dart/server/src/") projectile-globally-unignored-directories))
  (projectile-mode)
  (helm-projectile-on))

;; interactive edit (all same words in a buffer)
(use-package iedit
  :ensure t
  :pin melpa
  :bind ([C-f5] . iedit-mode))


(defun dv-show-file-name ()
  "Show the full path file name in the minibuffer."
  (interactive)
  (message "File path: %s" (buffer-file-name)))

(global-set-key [C-f1] 'dv-show-file-name)


;; Customize `highlight-indentation-face', and `highlight-indentation-current-column-face' to suit your theme.
(use-package highlight-indentation
  :ensure t
  :pin melpa
  :bind ([f5] . highlight-indentation-mode)
  :config
  (progn
    (custom-set-faces
     ;;'(highlight-indentation-face ((t (:background "#262626"))))
     '(highlight-indentation-face ((t (:background "magenta"))))
     )
    ;; (add-hook 'python-mode-hook
    ;;           (lambda () (interactive)
    ;;             (highlight-indentation-mode)))
    )
  )

(defun up-slightly () (interactive) (scroll-up 1))
(defun down-slightly () (interactive) (scroll-down 1))
(global-set-key (kbd "<mouse-4>") 'down-slightly)
(global-set-key (kbd "<mouse-5>") 'up-slightly)
; scroll one line at a time (less "jumpy" than defaults)
(setq scroll-step 1)

(use-package swift-mode
  :ensure t
  :pin melpa
  )

(semantic-mode t)

(setq savehist-additional-variables '(kill-ring search-ring regexp-search-ring))
(savehist-mode 1)

(use-package yasnippet
  :ensure t
  :pin melpa
  :config
  (progn
    (add-to-list 'yas-snippet-dirs (concat dotfiles-dir "snippets"))
    (yas-global-mode 1)
    )
  )

(use-package magit
  :ensure t
  :bind (("C-x g" . magit-status)))

;; YAML
(use-package yaml-mode
  :ensure t
  :mode "\\(\\.\\(yaml\\|yml\\)\\)\\'")

; xml pretty print
(defun xml-pretty-print (beg end &optional arg)
  "Reformat the region between BEG and END.
    With optional ARG, also auto-fill."
  (interactive "*r\nP")
  (let ((fill (or (bound-and-true-p auto-fill-function) -1)))
    (sgml-mode)
    (when arg (auto-fill-mode))
    (sgml-pretty-print beg end)
    (nxml-mode)
    (auto-fill-mode fill)))

;; git gutter
(use-package git-gutter
  :ensure t
  :pin melpa
  :init
  (progn
    (global-git-gutter-mode t)
    (when (< emacs-major-version 26)
      (git-gutter:linum-setup)))
  :bind
  (("C-x C-g" . git-gutter-mode))
  :config
  (progn
    (custom-set-variables
     '(git-gutter:modified-sign "  ")
     '(git-gutter:added-sign "++")
     '(git-gutter:deleted-sign "--"))
    (set-face-background 'git-gutter:modified "purple")
    (set-face-foreground 'git-gutter:added "green")
    (set-face-foreground 'git-gutter:deleted "red")))

(use-package qt-pro-mode
  :ensure t
  :pin melpa
  :mode ("\\.pro\\'" "\\.pri\\'"))

; never split windows
(setq pop-up-windows nil)

;;; Markdown preview
(use-package flymd
  :ensure t
  :bind ("C-c m" . flymd-flyit)
  )

;;; gist
(use-package gist
  :ensure t)

;;; editorconfig
(use-package editorconfig
  :ensure t
  :config
  (editorconfig-mode 1))

(use-package restclient
  :ensure t)

(use-package ggtags
  :ensure t
  :config
  (add-hook 'c-mode-common-hook
          (lambda ()
            (when (derived-mode-p 'c-mode 'c++-mode 'java-mode)
              (ggtags-mode 1))))
  )

(load (concat dotfiles-dir "init-python.el"))
(load (concat dotfiles-dir "init-cc.el"))

(cua-mode 1)               ;; C-c, C-v, C-x, C-z

;; Show the current function name in the header line
(which-function-mode)
(setq-default header-line-format
              '((which-func-mode ("" which-func-format " "))))
(setq mode-line-misc-info
            ;; We remove Which Function Mode from the mode line, because it's mostly
            ;; invisible here anyway.
      (assq-delete-all 'which-func-mode mode-line-misc-info))

(defun dv-copy-file-path-to-clipboard ()
  "Copy the current buffer file path to the clipboard."
  (interactive)
  (let ((filepath (if (equal major-mode 'dired-mode)
                      default-directory
                    (buffer-file-name))))
    (when filepath
      (kill-new filepath)
      (message "Copied buffer file name '%s' to the clipboard." filepath))))

(defun dv-copy-file-name-to-clipboard ()
  "Copy the current buffer file name to the clipboard."
  (interactive)
  (let ((filepath (if (equal major-mode 'dired-mode)
                      default-directory
                    (buffer-file-name))))
    (when filepath
      (setq filename (file-name-nondirectory filepath))
      (kill-new filename)
      (message "Copied buffer file name '%s' to the clipboard." filename))))

(use-package plantuml-mode
  :ensure t
  :config
  (progn
    (setq plantuml-jar-path (expand-file-name (concat dotfiles-dir "tools/plantuml.jar")))
    (setq plantuml-default-exec-mode 'jar))
  )

(use-package cmake-mode
  :ensure t
  )

;; server
(unless (and (boundp 'user-local-without-server)
             user-local-without-server)
  (unless (server-running-p)
    (progn
      (when (and (>= emacs-major-version 23)
                 (equal window-system 'w32))
        (defun server-ensure-safe-dir (dir) "Noop" t)) ; Suppress error "directory
                                        ; ~/.emacs.d/server is unsafe"
                                        ; on windows.
      (server-start)
      )
    )
  )
