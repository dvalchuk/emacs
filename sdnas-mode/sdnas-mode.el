;;; sdnas-mode.el --- sdnas integration for Emacs

;; Author: Denys Valchuk <denys_valchuk@DellTeam.com>
;; URL:
;; Version: 0.1.0

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Commentary:
;;
;; A minor mode which allows quick building of sdnas
;; projects with a few short key sequences.
;;
;;; Code:


(require 'projectile)

;;; Customization
(defgroup sdnas nil
  "sdnas integration"
  :prefix "sdnas-" :group 'tools)

(defcustom sdnas-mode-keymap-prefix "\C-c^"
  "Prefix for `sdnas-mode' commands."
  :group 'sdnas
  :type '(choice (const :tag "ESC"   "\e")
                 (const :tag "C-c ^" "\C-c^" )
                 (const :tag "none"  "")
                 string))

;;; Internal functions
(defun sdnas--run-build-cmd (cmd &optional dump-results-dir)
  "Call `./kobuild_standalone_sade.sh CMD' in the root of the project."
  (let* ((sdnas-root (concat (projectile-project-root) "sade"))
         (default-directory sdnas-root)
         (cmd (concat "cd " sdnas-root "; ./kobuild_standalone_sade.sh " cmd)))
    (save-some-buffers (not compilation-ask-about-save)
                       (lambda ()
                         (and default-directory
                              (string-prefix-p default-directory sdnas-root))))
    (when dump-results-dir
           (setq cmd (concat cmd  " && (pushd obj; ls -1rt *_playbook.tgz; popd)")))
    (setq compile-command cmd)
    (compilation-start cmd)))

(defun sdnas--run-root-cmd (cmd)
  "Call `CMD' in the root of the project."
  (let* ((sdnas-root (projectile-project-root))
         (default-directory sdnas-root)
         (cmd (concat "cd " sdnas-root "; " cmd)))
    (save-some-buffers (not compilation-ask-about-save)
                       (lambda ()
                         (and default-directory
                              (string-prefix-p default-directory sdnas-root))))
    (setq compile-command cmd)
    (compilation-start cmd)))

(defun sdnas--run-local-cmd (cmd)
  "Call `CMD' in current dir."
  (let* ((sdnas-root (projectile-project-root))
         (cmd (concat sdnas-root "sade/" cmd)))
    (save-some-buffers (not compilation-ask-about-save)
                       (lambda ()
                         (and default-directory
                              (string-prefix-p default-directory sdnas-root))))
    (setq compile-command cmd)
    (compilation-start cmd)))

(defun sdnas--run-tests-cmd (cmd)
  "Call `CMD' in the tests dir of the project."
  (let* ((sdnas-root (projectile-project-root))
         (tests-root (concat sdnas-root "sade/unit_tests/HA/"))
         (cmd (concat "cd " tests-root "; make " cmd))
         (default-directory tests-root)
         )
    (save-some-buffers (not compilation-ask-about-save)
                       (lambda ()
                         (and default-directory
                              (string-prefix-p default-directory sdnas-root))))
    (setq compile-command cmd)
    (compilation-start cmd)))


;;; User commands
(defun sdnas-sade ()
  "Build sdnas sade project."
  (interactive)
  (sdnas--run-build-cmd "sade"))

(defun sdnas-mkdir ()
  "Build sdnas current dir."
  (interactive)
  (sdnas--run-local-cmd "build_standalone_sade.sh"))

(defun sdnas-clean ()
  "Clean project."
  (interactive)
  (sdnas--run-build-cmd "cleanall"))

(defun sdnas-dependencies ()
  "Build sdnas dependencies."
  (interactive)
  (sdnas--run-build-cmd "sysinst")
  )

(defun sdnas-all ()
  "Build sdnas All project."
  (interactive)
  (sdnas--run-build-cmd "All" t))

(defun sdnas-container ()
  "Build sdnas container."
  (interactive)
  (sdnas--run-build-cmd "sade package container" t))

(defun sdnas-playbook ()
  "Build sdnas playbook."
  (interactive)
  (sdnas--run-build-cmd "sade package container playbook" t))

(defun sdnas-mkyml ()
  "Build sdnas mkyml."
  (interactive)
  (sdnas--run-build-cmd "checksys gennum container_base swagger_jar papi mf_common links prereqs makefile csx sade utils package" t))

(defun sdnas-mkymla ()
  "Build sdnas mkymla."
  (interactive)
  ;;  (sdnas--run-build-cmd        "setupwspace gennum swagger_jar papi mf_common prereqs makefile csx sade utils package container playbook" t))
    (message "sdnas-mkymla")
    (sdnas--run-build-cmd "checksys setupwspace gennum swagger_jar papi mf_common prereqs makefile csx sade utils package container playbook" t))
;;  (sdnas--run-build-cmd "checksys setupwspace gennum container_base swagger_jar papi mf_common prereqs makefile csx sade utils package container playbook" t))

(defun sdnas-mkymla-retail ()
  "Build sdnas playbook retail."
  (interactive)
  (sdnas--run-build-cmd "checksys setupwspace gennum swagger_jar papi mf_common prereqs makefile csx sade utils package container playbook --retail" t))

(defun sdnas-build-utests ()
  "Build sdnas tests."
  (interactive)
  (sdnas--run-tests-cmd "all"))

(defun sdnas-build-and-run-utests ()
  "Build&Run sdnas tests."
  (interactive)
  (sdnas--run-tests-cmd "all run"))

(defun sdnas-clean-utests ()
  "Clean sdnas tests."
  (interactive)
  (sdnas--run-tests-cmd "clean"))

;;; Minor mode
(defvar sdnas-command-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "s") #'sdnas-sade)
    (define-key map (kbd "d") #'sdnas-mkdir)
    (define-key map (kbd "c") #'sdnas-clean)
    (define-key map (kbd "D") #'sdnas-dependencies)
    (define-key map (kbd "a") #'sdnas-all)
    (define-key map (kbd "p") #'sdnas-playbook)
    (define-key map (kbd "t") #'sdnas-container)
    (define-key map (kbd "m") #'sdnas-mkyml)
    (define-key map (kbd "M") #'sdnas-mkymla)
    (define-key map (kbd "u") #'sdnas-build-utests)
    (define-key map (kbd "r") #'sdnas-build-and-run-utests)
    (define-key map (kbd "U") #'sdnas-clean-utests)
    map)
  "Keymap for sdnas mode commands after `sdnas-mode-keymap-prefix'.")
(fset 'sdnas-command-map sdnas-command-map)

(defvar sdnas-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map sdnas-mode-keymap-prefix 'sdnas-command-map)
    map)
  "Keymap for sdnas mode.")

(easy-menu-change
 '("Tools") "sdnas"
 '(["Build sade" sdnas-sade]
   ["Build current dir" sdnas-mkdir]
   ["Build sdnas playbook" sdnas-playbook]
   ["Build All" sdnas-all]
   ["Build sdnas container" sdnas-container]
   ["Build sdnas mkyml" sdnas-mkyml]
   ["Build sdnas mkymla" sdnas-mkymla]
   ["Build sdnas playbook retail" sdnas-mkymla-retail]
   "---"
   ["Build sdnas tests" sdnas-build-utests]
   ["Build&Run sdnas tests" sdnas-build-and-run-utests]
   ["Clean sdnas tests" sdnas-clean-utests]
   "---"
   ["Build dependencies" sdnas-dependencies]
   ["Clean Project" sdnas-clean]))

;;;###autoload
(defun sdnas-conditionally-enable ()
  "Enable `sdnas-mode' only when a `Version.sdnas.xml' file is present in project root."
  (when (ignore-errors (projectile-project-root))
    (sdnas-mode 1)
    (setq inhibit-local-variables t)
    (setq enable-local-variables nil)
    )
  )

;;;###autoload
(define-minor-mode sdnas-mode
  "sdnas integration."
  :lighter " sdnas"
  :keymap sdnas-mode-map
  :group 'sdnas
  :require 'sdnas)

(provide 'sdnas-mode)
;;; sdnas-mode.el ends here
