#!/usr/bin/env python

import os
from stat import S_ISSOCK

serverdir = os.path.join(os.environ.get('TMPDIR', '/tmp'),
                         'emacs{0}'.format(os.geteuid()))
servers = [s for s in os.listdir(serverdir)
           if S_ISSOCK(os.stat(os.path.join(serverdir, s)).st_mode)]

for s in servers:
    print(s)
