#!/bin/sh

LOCAL=${HOME}/local
if [ ! -d "${LOCAL}" ] ; then
    mkdir ${LOCAL}
fi
mkdir ${LOCAL}/tmp

cd ${LOCAL}/tmp

wget http://ftp.gnu.org/gnu/emacs/emacs-26.3.tar.gz

tar xvfz *.tar.gz

cd ${LOCAL}/tmp/emacs-26.3

./configure  --prefix=${LOCAL}/emacs/support/ --bindir=${LOCAL}/emacs/ --with-gif=no --without-x

make && make install

rm -rf ${LOCAL}/tmp
